import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'account_page.dart';
import 'cart_page.dart';
import 'home_page.dart';
import 'notifications_page.dart';

class ButtonNavigation extends StatefulWidget {
  const ButtonNavigation({Key? key}) : super(key: key);

  @override
  State<ButtonNavigation> createState() => _ButtonNavigationState();
}

class _ButtonNavigationState extends State<ButtonNavigation> {
  int _selectedIndex = 0;

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  final List<Widget> _widgetoptions = [
    const HomePage(),
    const Notifications(),
    const Account(),
    const CartPage(),
  ];

  void _onItemTapped(int index){
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetoptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home,color: Colors.black), label: "Home"),
          BottomNavigationBarItem(
              icon: Icon(Icons.notifications,color: Colors.black), label: "Notifications"),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_rounded,color: Colors.black), label: "Account"),
          BottomNavigationBarItem(icon: Icon(Icons.shopping_cart,color: Colors.black),label: "Cart")
        ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          onTap: _onItemTapped
      ),
    );
  }
}
