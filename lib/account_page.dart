import 'package:amazon/Edit_profile.dart';
import 'package:amazon/buttonnavigation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Model/UserModel.dart';
import 'login.dart';

class Account extends StatefulWidget {
  const Account({Key? key}) : super(key: key);

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {
  UserModel? UsersDataModel;

  @override
  void initState() {
    super.initState();
    retriveData().then((value) {
      UsersDataModel = value;
      setState(() {});
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CupertinoColors.activeGreen,
        title: const Text(
          "Account",
          style: TextStyle(
            color: CupertinoColors.black,
          ),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.settings,
                color: Colors.black,
              ))
        ],
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          tooltip: 'Menu Icon',
          onPressed: () {},
        ),
      ),


      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  width: 150,
                  height: 150,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: CircleAvatar(
                      child: SizedBox(
                        width: 150,
                        height: 150,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(90),
                          child: Image.network(
                            "https://media.licdn.com/dms/image/D4D03AQGWVY4uzYHZWg/profile-displayphoto-shrink_800_800/0/1665047561445?e=1690416000&v=beta&t=5ES0zzmaYhAn-P6XiYFrJGZk5YlwSxd1DTKID5sW0nE",
                            width: 890,
                            height: 890,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("   ${UsersDataModel?.name}",
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 19)),
                    Padding(
                      padding: const EdgeInsets.only(left: 16, top: 5),
                      child: Text("${UsersDataModel?.email}",
                          style: TextStyle(fontWeight: FontWeight.w400)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16,top: 15),
                      child: ElevatedButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile(),));
                      }, child: Text("Edit Profile")),
                    )
                  ],
                )
              ],
            ),
            Column(
              children:  [
                Divider(thickness: 2,),
                const ListTile(
                  leading: Icon(Icons.heart_broken),
                  title: Text("Favorites"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
                const ListTile(
                  leading: Icon(Icons.file_download_outlined),
                  title: Text("Downloads"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
                Divider(thickness: 2,),

                const ListTile(
                  leading: Icon(Icons.language),
                  title: Text("Language"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),


                  const ListTile(
                    leading: Icon(Icons.location_on_outlined
                    ),
                    title: Text("Location"),
                    trailing: Icon(Icons.arrow_forward_ios_rounded),
                  ),

                const ListTile(
                  leading: Icon(Icons.smart_display
                  ),
                  title: Text("Display"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
                const ListTile(
                  leading: Icon(Icons.subscriptions_rounded
                  ),
                  title: Text("Subscripion"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
                Divider(thickness: 2,),

                const ListTile(
                  leading: Icon(Icons.delete_forever),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text("Clear Cache"),
                ),

                  const ListTile(
                  leading: Icon(Icons.access_time_rounded),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text("Clear Cache"),
                ),

                ListTile(
                  onTap: (){
                    FirebaseAuth.instance.signOut();
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage(),));
                  },
                  leading: Icon(Icons.login_sharp,color: CupertinoColors.destructiveRed),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text("Logout"),
                ),

              ],
            )
          ],
        ),
      ),
    );
  }

  Future<UserModel?> retriveData() async {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    var user =
        await FirebaseFirestore.instance.collection("Users").doc(auth).get();
    var userModel = UserModel.fromJson(user.data()!);
    return userModel;
  }
}
