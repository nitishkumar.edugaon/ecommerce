import 'package:amazon/phone_auth/phone_number.dart';
import 'package:amazon/singnup.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';


import 'buttonnavigation.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  singup() async {
    var mAuth = FirebaseAuth.instance;
    try {
      await mAuth.signInWithEmailAndPassword(
          email: email.text, password: password.text);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => ButtonNavigation(),));
      Fluttertoast.showToast(msg: "Successfuly singup");
    }
    catch (e) {
      Fluttertoast.showToast(msg: "Invalid email and password");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: [
        Container(
          height: 280,
          width: 200,
          padding: EdgeInsets.only(top: 15),
          child: const Center(
              child: Image(
                image: AssetImage('assets/images/online_shopping.png'),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 32, right: 32),
          child: TextField(
            controller: email,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.orange),
                  borderRadius: BorderRadius.circular(10)),
              hintText: "Email Id",
              labelText: "Enter Your Email",
              hintStyle:
              const TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
              labelStyle:
              const TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30, left: 32, right: 32),
          child: TextField(
            controller: password,
            decoration: InputDecoration(
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: "Password",
                labelText: "Enter your password",
                labelStyle: const TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
        ),

        const Padding(
          padding: EdgeInsets.only(left: 230, top: 15),
          child: Text(
            'Forgot Password?',
            style: TextStyle(
                color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          width: 30,
          height: 85,
          child: Padding(
            padding: const EdgeInsets.only(top: 38, left: 60, right: 60),
            child: ElevatedButton(
                onPressed: () {
                  singup();
                },
                child: const Text(
                  'Login',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
            ),
          ),
        ),

        const Padding(
          padding: EdgeInsets.only(left: 199, top: 50),
          child: Text(
            "-OR-",
            style: TextStyle(fontSize: 18),
          ),
        ),


        SizedBox(
          height: 150,
          width: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [


              InkWell(
                onTap: (){
                  signInWithGoogle().then((value) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ButtonNavigation(),));
                  });
                },
                child: Padding(
                  padding: EdgeInsets.only(top: 25),
                  child: Image.asset('assets/images/google.png',
                      width: 70, height: 70),
                ),
              ),


              InkWell(
                onTap: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => PhoneNumberPage(),));
                },
                child: Padding(
                    padding: EdgeInsets.only(top: 25),
                    child: Image.asset(
                      "assets/images/otp.png",
                      width: 90,
                      height: 90,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25),
                child: Image.asset(
                  "assets/images/Facebook.png",
                  width: 70,
                  height: 70,
                ),
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'New User?',
                style: TextStyle(fontSize: 15),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SingUpPage(),),);
                },
                child: const Text(
                  ' SingUp',
                  style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue),
                ),
              ),
            ],
          ),
        ),

      ]),
    );
  }
  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }
}

