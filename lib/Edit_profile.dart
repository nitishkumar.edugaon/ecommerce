import 'package:amazon/Model/UserModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'account_page.dart';

class EditProfile extends StatefulWidget {
  EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();

  UserModel? UsersDataModel;

  @override
  void initState() {
    super.initState();
    retriveData().then((value){
      setState(() {
       UsersDataModel = value;
       name.text=UsersDataModel?.name??"";
       email.text=UsersDataModel?.email??"";
       phone.text=UsersDataModel?.phone??"";
     });
    });
  }


  void updateUser() async {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    // await uploadImageToFirebase();
    FirebaseFirestore.instance.collection("Users").doc(auth).update({
      "name": name.text.trim(),
      "email": email.text.trim(),
      "phone": phone.text.trim(),
      // "imageUrl": imageUrl,
    }).then((value) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) =>  const Account(),),);
      Fluttertoast.showToast(msg: "Updated successfully");
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Center(
              child: Text(
            "Edit Profile",
            style: TextStyle(color: Colors.black),
          )),
          leading: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back_ios_new,
                color: Colors.black,
              )),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.check,
              ),
              color: Colors.green,
            ),
          ],
        ),
        body: ListView(children: [
          Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Column(children: [
                Stack(
                  children: [
                    // imageFile != null ?
                    // CircleAvatar(
                    //     radius: 50,
                    //     backgroundImage: FileImage(File(imageFile!.path))
                    // ):
                    const CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage(
                          "https://firebasestorage.googleapis.com/v0/b/flutterbasic-a5dcc.appspot.com/o/usersImage%2Fpic.webp?alt=media&token=6141432e-12ce-4633-a270-dfcecc902371"),
                      // widget.imagePath != null ? FileImage(File(imageFile!.path)) : null,
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.white,
                        ),
                        child: IconButton(
                          icon: const Icon(Icons.camera_alt),
                          onPressed: () {
                            showModalBottomSheet(
                              context: context,
                              builder: (BuildContext context) {
                                return Wrap(
                                  children: [
                                    ListTile(
                                      leading: const Icon(
                                        Icons.camera_alt,
                                        color: Colors.black,
                                      ),
                                      title: const Text('Camera'),
                                      onTap: () {
                                        // pickImage(ImageSource.camera);
                                        Navigator.pop(context);
                                      },
                                    ),
                                    ListTile(
                                      leading: const Icon(
                                        Icons.image,
                                        color: Colors.black,
                                      ),
                                      title: const Text('Gallery'),
                                      onTap: () {
                                        // pickImage(ImageSource.gallery);
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ])),
          Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 20, right: 230),
                child: Text("Your Information",
                    style:
                        TextStyle(fontSize: 19, fontWeight: FontWeight.w500)),
              ),
              Padding(
                padding: const EdgeInsets.all(27),
                child: TextField(
                  controller: name,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: "First name"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 26,
                  right: 26,
                ),
                child: TextField(
                  controller: phone,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: "Phone"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26, top: 29),
                child: TextField(
                  controller: email,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: "Email Id"),
                ),
              ),

              SizedBox(
                height: 90,
                width: 180,
                child: Padding(
                  padding: const EdgeInsets.only(top: 36),
                  child: ElevatedButton(
                    onPressed: () {
                      updateUser();
                    },
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                    child: const Text(
                      "Update",
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19,
                      ),
                    ),
                  ),
                ),
              )

            ],
          )
        ]));
  }

  Future<UserModel?>retriveData() async{
    var auth = FirebaseAuth.instance.currentUser?.uid;
    var user = await FirebaseFirestore.instance.collection("Users").doc(auth).get();
    var userModel = UserModel.fromJson(user.data()!);
    return userModel;
  }
}
