import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'buttonnavigation.dart';
import 'login.dart';

class SingUpPage extends StatefulWidget {
  const SingUpPage({Key? key}) : super(key: key);

  @override
  State<SingUpPage> createState() => _SingUpPageState();
}

class _SingUpPageState extends State<SingUpPage> {
  TextEditingController _Entername = TextEditingController();
  TextEditingController _Enteremail = TextEditingController();
  TextEditingController _Enterpassword = TextEditingController();
  TextEditingController _Enterphone = TextEditingController();

  validate() {
    if (_Entername.text.length <= 3) {
      Fluttertoast.showToast(msg: 'Please enter name');
    } else if (!_Enteremail.text.contains("@gmail.com")) {
      Fluttertoast.showToast(msg: "Please enter email");
    } else if (_Enterpassword.text.length <= 6) {
      Fluttertoast.showToast(msg: "Please enter password");
    } else if (_Enterphone.text.length < 10) {
      Fluttertoast.showToast(msg: "Please enter phone number");
    } else {
      emailAuth();
    }
  }

  emailAuth() {
    FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: _Enteremail.text, password: _Enterpassword.text)
        .then((value){
      UserDate();
      Fluttertoast.showToast(msg: "EmailAuth success");
    });
  }

  UserDate(){
    var Auth = FirebaseAuth.instance.currentUser?.uid;
    FirebaseFirestore.instance.collection("Users").doc(Auth).set({
      "name": _Entername.text,
      "email": _Enteremail.text,
      "password": _Enterpassword.text,
      "phone": _Enterphone.text,
      }).then((value) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ButtonNavigation(),));
        Fluttertoast.showToast(msg: "successfuly",);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.only(top: 110, left: 130, right: 130),
          child: Text(
            'Sing Up Now',
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 80, left: 30, right: 40),
          child: TextField(
            keyboardType: TextInputType.text,
              controller: _Entername,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25)),
                  hintText: "Enter Name")),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, left: 30, right: 40),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
              controller: _Enteremail,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25)),
                  hintText: "Enter Email")),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, left: 30, right: 40),
          child: TextField(
            keyboardType: TextInputType.visiblePassword,
              controller: _Enterpassword,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25)),
                  hintText: "Enter Password")),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, left: 30, right: 40),
          child: TextField(
            keyboardType: TextInputType.phone,
              controller: _Enterphone,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25)),
                  hintText: "Enter Phone.No")),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 60, right: 60, top: 70),
          child: ElevatedButton(
            onPressed: () {

              validate();
            },
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              shape: StadiumBorder(),
            ),
            child: Text(
              "Sing Up",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 70),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Already have an Account?',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ));
                },
                child: Text(
                  " Sing In",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
              )
            ],
          ),
        )
      ]),
    );
  }
}
