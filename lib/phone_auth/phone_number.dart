import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'otp_page.dart';

class PhoneNumberPage extends StatefulWidget {
  const PhoneNumberPage({Key? key}) : super(key: key);

  @override
  State<PhoneNumberPage> createState() => _PhoneNumberPageState();
  static String verify = "";
}

class _PhoneNumberPageState extends State<PhoneNumberPage> {
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(30),
          child: TextField(
            controller: phoneController,
            keyboardType: TextInputType.phone,
            maxLength: 10,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                labelText: "Enter your Phone Number"
            ),
          ),
        ),

        ElevatedButton(
            onPressed: () async {
              try {
                await FirebaseAuth.instance.verifyPhoneNumber(
                  phoneNumber: '+91${phoneController.text}',

                  verificationCompleted: (PhoneAuthCredential credential) {},
                  verificationFailed: (FirebaseAuthException e) {},
                  codeSent: (String verificationId, int? resendToken) {
                    PhoneNumberPage.verify=verificationId;
                    Navigator.pushReplacement(
                        context, MaterialPageRoute(builder: (context) =>OtpPage()));
                    Fluttertoast.showToast(msg: "Sent OTP");
                  },
                  codeAutoRetrievalTimeout: (String verificationId) {

                  },
                );
              }
              catch (e) {
                Fluttertoast.showToast(msg: "OTP Failed");
              }
            },
            child: const Text("Send Otp")
        )
      ],
    ),
    );
  }
}
