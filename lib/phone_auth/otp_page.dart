import 'package:amazon/phone_auth/phone_number.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pinput/pinput.dart';

import '../buttonnavigation.dart';
import '../home_page.dart';

class OtpPage extends StatefulWidget {
  const OtpPage({Key? key}) : super(key: key);

  @override
  State<OtpPage> createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController pinController = TextEditingController();

  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(

        body: Padding(
          padding: const EdgeInsets.all(50),
          child: Column(
            children: [
              Pinput(
                keyboardType: TextInputType.number,
                controller: pinController,
                length: 6,
                showCursor: true,
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                textInputAction: TextInputAction.next,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: ElevatedButton(
                    onPressed: () async {
                      try {
                        PhoneAuthCredential credential = PhoneAuthProvider.credential(
                            verificationId: PhoneNumberPage.verify,
                            smsCode: pinController.text);
                        // Sign the user in (or link) with the credential
                        await auth.signInWithCredential(credential);
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  ButtonNavigation()));
                        Fluttertoast.showToast(msg: "Login successful");
                      } catch (e) {
                        Fluttertoast.showToast(msg: "Enter Valid OTP");
                      }
                    },
                    child: const Text("Enter Otp")),
              )
            ],
          ),
        ),
      ),
    );
  }
}